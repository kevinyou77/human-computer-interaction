let manual      = document.querySelector("#manual"),
    auto        = document.querySelector("#auto");
    colorSelect = document.querySelector("#color-select"),
    carSelect   = document.querySelector("#car-select"),
    address     = document.querySelector("#address"),
    email       = document.querySelector("#email"),
    phone       = document.querySelector("#phone"),
    submit      = document.querySelector("#submit"),
    errorMsg    = document.querySelectorAll(".error-msg"),
    quantity    = document.querySelector("#quantity");

function validate() {
    let carTxt        = carSelect.options[carSelect.selectedIndex].value,
        colorTxt      = colorSelect.options[colorSelect.selectedIndex].value,
        addressTxt    = address.value,
        phoneTxt      = phone.value,
        manualChk     = manual.checked,
        autoChk       = auto.checked,
        emailTxt      = email.value,
        quantityTxt   = quantity.value;

    if (carTxt === "")
        errorMsg[0].innerText = "Car not selected";
    else
        errorMsg[0].innerText = "";

    if (!manualChk && !autoChk) 
        errorMsg[1].innerText = "Transmission not selected";
    else 
        errorMsg[1].innerText = "";
    
    if (colorTxt === "") 
        errorMsg[2].innerText = "Color not selected";
    else 
        errorMsg[2].innerText = "";

    if (emailTxt === "") 
        errorMsg[3].innerText = "Email must be filled";
    else 
        errorMsg[3].innerText = "";
    
    if (phoneTxt === "") 
        errorMsg[4].innerText = "Phone must be filled";
    else 
        errorMsg[4].innerText = "";
    
    if (addressTxt === "") 
        errorMsg[5].innerText = "Address must be filled";
    else
        errorMsg[5].innerText = "";

    if (quantityTxt === "" || quantityTxt === "0") 
        errorMsg[6].innerText = "Quantity must be filled";
    else
        errorMsg[6].innerText = "";

    for (let i = 0; i < errorMsg.length; i++) 
        if (errorMsg[i].innerText === "") return false;
    
    return true;
}

function responsiveNavbar() {
    let x = document.getElementById("navigation-menu-top");
    if (x.className === "navigation-menu") {
        x.className += " responsive";
    } else {
        x.className = "navigation-menu";
    }
}